class ReadingsController < ApplicationController
  include ActionController::Live

  before_action :set_reading, only: [:show, :edit, :update, :destroy]

	def watch

	end


	def index_stream
		response.headers['Content-Type'] = 'text/event-stream'		
 		sse = SSE.new(response.stream, retry: 300, event: "results")

		@readings = Reading.all
		begin
			@readings.each do |reading|
				sse.write(reading)
			end
		rescue IOError => e
			# When the client disconnects, we'll get an IOError on write
		ensure
			sse.close
		end

	end

	# GET /readings
  # GET /readings.json
  def latest_readings
    @readings = Reading.order('time DESC').limit(100)
    respond_to do |format|
      format.html
    end
  end


  # GET /readings
  # GET /readings.json
  def index
		@search = Reading.search(params[:q])
    @readings = @search.result.order('time DESC').paginate(:page => params[:page])
    respond_to do |format|
      format.html
      format.csv { send_data Reading.to_csv }
    end
  end

  # GET /readings/1
  # GET /readings/1.json
  def show
  end

  # GET /readings/new
  def new
    @reading = Reading.new
  end

  # GET /readings/1/edit
  def edit
  end

  # POST /readings
  # POST /readings.json
  def create
    @reading = Reading.new(reading_params)

    respond_to do |format|
      if @reading.save
        format.html { redirect_to @reading, notice: 'Reading was successfully created.' }
        format.json { render action: 'show', status: :created, location: @reading }
      else
        format.html { render action: 'new' }
        format.json { render json: @reading.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /readings/1
  # PATCH/PUT /readings/1.json
  def update
    respond_to do |format|
      if @reading.update(reading_params)
        format.html { redirect_to @reading, notice: "Reading update successful." }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @reading.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /readings/1
  # DELETE /readings/1.json
  def destroy
    @reading.destroy
    respond_to do |format|
      format.html { redirect_to readings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reading
      @reading = Reading.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reading_params
	  	# Needed to convert strings to integers so that Rails can deal with an array of checkboxes.
      params.require(:reading).permit(:time, :tag_epc, :antenna)
    end
end

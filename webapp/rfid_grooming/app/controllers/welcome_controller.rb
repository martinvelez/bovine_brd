class WelcomeController < ApplicationController

	# GET /
  def index
		@cows = []
		@heartbeats = []
		# get active cows
		if current_user
			@cows = Cow.where("NOW() BETWEEN date_trunc('day', date_in) AND date_trunc('day', date_out)")
			@heartbeats = Cow.where(:name => ["Heartbeat 1", "Heartbeat 2"])
		end
  end

	# AJAX request handler
	def get_latest_reading
		if current_user
			cow_id = params["id"].to_i	
			cow = Cow.find(cow_id)	
			if !cow.nil?
				respond_to do |format|
					format.json { render json: cow.latest_reading }
				end
			end
		end
	end

end

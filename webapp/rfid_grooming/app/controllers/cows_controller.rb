class CowsController < ApplicationController
  before_action :set_cow, only: [:show, :edit, :update, :destroy]

  # GET /cows
  # GET /cows.json
  def index
    @cows = Cow.order('id')
  end

  # GET /cows/1
  # GET /cows/1.json
  def show
    if params["from"].nil? or params["to"].nil?
      if !@cow.get_readings["All Tags"].empty?         
        @from_date = @cow.get_readings["All Tags"].first.time
        @to_date =  @cow.get_readings["All Tags"].last.time
      else
        @from_date =  DateTime.now
        @to_date =  DateTime.now
      end
    else                                                                            
      @from_date = DateTime.new(params["from"]["(1i)"].to_i,                        
        params["from"]["(2i)"].to_i,                                                
        params["from"]["(3i)"].to_i,                                                
        params["from"]["(4i)"].to_i,                                                
        params["from"]["(5i)"].to_i)                                                
      @to_date = DateTime.new(params["to"]["(1i)"].to_i,                            
        params["to"]["(2i)"].to_i,                                                  
        params["to"]["(3i)"].to_i,                                                  
        params["to"]["(4i)"].to_i,                                                  
        params["to"]["(5i)"].to_i)                                                  
    end # for if-else                                                               
                                                                                    
                                                                                    
    @readings = @cow.filter(@from_date, @to_date, [0,1,2,3])["All Tags"]
    @readings = @readings.paginate(:page => params[:page], :per_page => 100)

    respond_to do |format|                                                          
      format.html                                                                   
      format.csv { send_data Cow.to_csv_for_a_cow(@cow.id)}
    end 
  end

  # GET /cows/new
  def new
    @cow = Cow.new
  end

  # GET /cows/1/edit
  def edit
  end

  # POST /cows
  # POST /cows.json
  def create
		date_in = DateTime.new(params["from"]["(1i)"].to_i,                             
      params["from"]["(2i)"].to_i,                                                  
      params["from"]["(3i)"].to_i,                                                  
      params["from"]["(4i)"].to_i,                                                  
      params["from"]["(5i)"].to_i)                                                  
    date_out = DateTime.new(params["to"]["(1i)"].to_i,                              
      params["to"]["(2i)"].to_i,                                                    
      params["to"]["(3i)"].to_i,                                                    
      params["to"]["(4i)"].to_i,                                                    
      params["to"]["(5i)"].to_i)     
    @cow = Cow.new(cow_params)
		@cow.date_in = date_in
		@cow.date_out = date_out

    respond_to do |format|
      if @cow.save
        format.html { redirect_to @cow, notice: 'Cow was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cow }
      else
        format.html { render action: 'new' }
        format.json { render json: @cow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cows/1
  # PATCH/PUT /cows/1.json
  def update
		date_in = DateTime.new(params["from"]["(1i)"].to_i,                             
      params["from"]["(2i)"].to_i,                                                  
      params["from"]["(3i)"].to_i,                                                  
      params["from"]["(4i)"].to_i,                                                  
      params["from"]["(5i)"].to_i)                                                  
    date_out = DateTime.new(params["to"]["(1i)"].to_i,                              
      params["to"]["(2i)"].to_i,                                                    
      params["to"]["(3i)"].to_i,                                                    
      params["to"]["(4i)"].to_i,                                                    
      params["to"]["(5i)"].to_i)     
		params["cow"]["date_in"] = date_in
		params["cow"]["date_out"] = date_out
    respond_to do |format|
      if @cow.update(cow_params)
        format.html { redirect_to @cow, notice: 'Cow was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cows/1
  # DELETE /cows/1.json
  def destroy
    @cow.destroy
    respond_to do |format|
      format.html { redirect_to cows_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cow
      @cow = Cow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cow_params
      params.require(:cow).permit(:name, :pen, :date_in, :date_out)
    end
end

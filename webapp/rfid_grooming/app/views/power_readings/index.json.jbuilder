json.array!(@power_readings) do |power_reading|
  json.extract! power_reading, :id, :time, :power, :pen
  json.url power_reading_url(power_reading, format: :json)
end

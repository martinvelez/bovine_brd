json.array!(@tags) do |tag|
  json.extract! tag, :id, :epc, :type, :cow_id, :cow_position_id, :status
  json.url tag_url(tag, format: :json)
end

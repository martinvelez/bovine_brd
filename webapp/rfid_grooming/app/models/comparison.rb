class Comparison < ActiveRecord::Base
	serialize :comparison, Array

	def self.to_csv(id)
		comparison = Comparison.find(id)
		CSV.generate do |csv|
			comparison.comparison.each do |row|
				csv << row
			end
		end
	end

end

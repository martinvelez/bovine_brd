class Cow < ActiveRecord::Base
	has_many :tags
	attr_reader :readings, :intervals

	def tag_epcs
		tag_epcs = []
		self.tags.each do |tag|
			tag_epcs.push(tag.epc)
		end 
		return tag_epcs
	end

	def latest_reading
		#reading = {}
		latest_reading = Reading.where(:tag_epc => self.tag_epcs).order("time DESC").first
		#reading["id"] = latest_reading.id
		#reading["date"] = latest_reading.time.to_date.to_s
		#reading["time"] = latest_reading.time.to_date.to_s
		return latest_reading
	end

	def get_readings
		self.tags.each do |tag|
				tag.readings.each do |reading|
					@readings_by_position["All Tags"]<< reading
				end
		end # End of tags loop

		@readings_by_position.each do |pos, readings|
			readings.sort!{ |r1,r2| r1.time <=> r2.time }
		end
	end

	def filter (from_date, to_date, antennas)
		self.tags.each do |tag|
			#puts tag.epc
			if !from_date.nil? and !to_date.nil? and !antennas.nil?
				#puts "FILTERED: !from_date.nil? and !to_date.nil? and !antennas.nil?"
				# filter by antenna
				tag_readings = tag.readings.to_ary
				#puts "tag_readings.size = #{tag_readings.size}"
				tag_readings.delete_if{ |reading| !antennas.include?(reading.antenna) }
				#puts "Filtered by antennas: tag_readings.size = #{tag_readings.size}"
				# filter by time
				tag_readings.delete_if{ |reading| reading.time < from_date || to_date < reading.time }
				#puts "Filtered by date range: tag_readings.size = #{tag_readings.size}"
				# store
				tag_readings.each do |reading|
					@readings_by_position[tag.position] << reading
					@readings_by_position["All Tags"]<< reading
				end
			else # No filter
				#puts "NO FILTER: NOT (!from_date.nil? and !to_date.nil? and !antennas.nil?)"
				tag.readings.each do |reading|
					@readings_by_position[tag.position] << reading
          @readings_by_position["All Tags"]<< reading
				end
			end # End of if-else block
		end # End of tags loop

		@readings_by_position.each do |pos, readings|
			#puts "#{pos} = #{readings.size}"
			readings.sort!{ |r1,r2| r1.time <=> r2.time }
		end
	end


	# Assume that reading times are sorted
	def set_intervals(max_gap_size=1, from_date=nil, to_date=nil, antennas=nil)
		self.filter(from_date, to_date, antennas)
		
		#puts @readings_by_position["All Tags"].size # debug
		if !@readings_by_position["All Tags"].empty?
			
			# Initialize an interval with the first point
			# Leave it open
			@readings_by_position.each do |position, readings|
					next if readings.empty?
					interval = [readings[0].time, readings[0].time]  

					if readings.size >= 2	

						for i in 1..readings.size-1 do 
							if readings[i].time - readings[i-1].time <= max_gap_size 
								# Merge this point with the open interval
								interval[1] = readings[i].time
							else 
								# This point should be the starting point of another interval.
								# Close open interval first, then start a new interval.
								interval[1] = readings[i-1].time
								@intervals[position] << interval
								interval = [readings[i].time, readings[i].time]
							end	# if - the next reading is within gap size

							# If this is the last point, then we should save the interval since we
							# we do not do another loop iteration.
							if i == readings.size - 1 
								@intervals[position] << interval
							end 
						end # for - the rest of the readings
					end # if - there is more than two readings
			end # each reading by position 
		end # if - readings is NOT empty
	end # def - intervals


	def self.to_csv(options={})
		CSV.generate do |csv|                                                       
			csv << ["Cow Name", "Antennas", "Position", "Gap Size", "Start Time", "End Time", "Duration"]
			all.each do |cow|                                                        
				cow.set_intervals(options[:max_gap_size], options[:from_date], options[:to_date], options[:antennas])
				cow.intervals.each do |position, intervals|                             
					intervals.each do |interval|                                          
						csv << [cow.name, options[:antennas], position, options[:max_gap_size], interval[0].to_s(:db), interval[1].to_s(:db), (interval[1] - interval[0]).to_s(:db)]
					end                                                                   
				end                                                                     
			end                                                                       
		end # CSV generate 	
	end

	def self.to_csv_for_a_cow(id)
		cow = Cow.find(id)
		CSV.generate do |csv|
			csv << ["cow_id",
				"cow_name",
				"pen",
				"tag_epc",
				"antenna",
				"antenna_attenuation",
				"cow_position"
				 ]
			#if !cow.get_readings["All Tags"].empty?
				cow.get_readings["All Tags"].each do |reading|
					csv << [cow.id, cow.name ,cow.pen, reading.tag_epc, reading.antenna, reading.antenna_attenuation, reading.tag.position] 
			  end
			#end
		end
	end


	def select_option
		"#{id} - #{name}"
	end # select_option


	after_initialize do |cow|
		@intervals = {}
		@intervals["All Tags"] = []
		@readings_by_position = {}
		@readings_by_position["All Tags"] = [] 
		self.tags.each do |tag|
			if !tag.position.nil?
				@readings_by_position[tag.position] = [] if @readings_by_position[tag.position].nil?
				@intervals[tag.position] = [] if @intervals[tag.position].nil?
			end
		end
	end


end # model

class AddDateOnToTags < ActiveRecord::Migration
  def change
    add_column :tags, :date_on, :timestamp
  end
end

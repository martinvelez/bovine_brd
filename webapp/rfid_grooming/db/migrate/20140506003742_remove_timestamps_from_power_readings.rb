class RemoveTimestampsFromPowerReadings < ActiveRecord::Migration
  def change
    remove_column :power_readings, :created_at, :string
    remove_column :power_readings, :updated_at, :string
  end
end

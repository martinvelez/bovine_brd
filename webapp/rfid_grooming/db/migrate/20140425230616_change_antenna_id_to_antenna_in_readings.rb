class ChangeAntennaIdToAntennaInReadings < ActiveRecord::Migration
  def change
		rename_column :readings, :antenna_id, :antenna
  end
end

class RenamePositionIdToCowPositionIdInTags < ActiveRecord::Migration
  def change
		rename_column :tags, :position_id, :cow_position_id
  end
end

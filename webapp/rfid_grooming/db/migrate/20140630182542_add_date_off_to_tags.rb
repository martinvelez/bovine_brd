class AddDateOffToTags < ActiveRecord::Migration
  def change
    add_column :tags, :date_off, :timestamp
  end
end

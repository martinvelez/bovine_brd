class RemoveStatusFromTags < ActiveRecord::Migration
	def up
		remove_column :tags, :status
	end

  def down 
		add_column :tags, :status, :string
  end
end

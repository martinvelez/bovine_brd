class AddIndexToTags < ActiveRecord::Migration
  def change
		add_index :tags, :epc, unique: true
  end
end

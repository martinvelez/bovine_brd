class ChangePowerInPowerReadings < ActiveRecord::Migration
  def change
		change_column :power_readings, :power, :decimal, precision: 5, scale: 2
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140813080229) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "antenna_placements", force: true do |t|
    t.datetime "from_date"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "antenna_id"
  end

  add_index "antenna_placements", ["antenna_id"], name: "index_antenna_placements_on_antenna_id", using: :btree

  create_table "antennas", force: true do |t|
    t.string   "antenna"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "port"
  end

  create_table "comparisons", force: true do |t|
    t.text     "comparison"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "filename"
  end

  create_table "cow_positions", force: true do |t|
    t.string   "cow_position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cows", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pen"
    t.datetime "date_in"
    t.datetime "date_out"
  end

  create_table "heartbeats", force: true do |t|
    t.integer  "pen"
    t.datetime "time", default: "now()"
  end

  create_table "power_readings", force: true do |t|
    t.datetime "time"
    t.integer  "power"
    t.integer  "pen"
  end

  create_table "readings", force: true do |t|
    t.datetime "time"
    t.string   "tag_epc"
    t.integer  "antenna"
    t.integer  "antenna_attenuation"
  end

  add_index "readings", ["tag_epc"], name: "index_readings_on_tag_epc", using: :btree

  create_table "tags", force: true do |t|
    t.string   "epc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tag_type"
    t.integer  "cow_id"
    t.integer  "cow_position_id"
    t.string   "position"
    t.datetime "date_on"
    t.datetime "date_off"
  end

  add_index "tags", ["epc"], name: "index_tags_on_epc", unique: true, using: :btree

  create_table "temp", id: false, force: true do |t|
    t.datetime "time"
    t.string   "tag_epc"
    t.integer  "antenna"
    t.integer  "antenna_attenuation"
  end

  create_table "temp_tags", id: false, force: true do |t|
    t.integer  "id"
    t.string   "epc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tag_type"
    t.integer  "cow_id"
    t.integer  "cow_position_id"
    t.string   "position"
  end

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
  end

end

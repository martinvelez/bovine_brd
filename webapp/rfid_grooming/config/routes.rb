require 'sidekiq/web'

RfidGrooming::Application.routes.draw do
  resources :heartbeats

  resources :power_readings
	post 'power_readings/upload' => 'power_readings#upload'

	get 'readings/stream' => 'readings#index_stream'
	get 'readings/watch' => 'readings#watch'
	get 'readings/latest_readings' => 'readings#latest_readings'

  resources :comparisons
	post 'comparisons/upload' => 'comparisons#upload'

  get 'intervals' => 'intervals#index'
  get "welcome/index"

  resources :readings  
  resources :tags
  resources :cows

	get 'welcome/get_latest_reading' => 'welcome#get_latest_reading'
	get 'welcome/get_latest_reading_by_pen' => 'welcome#get_latest_reading_by_pen'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  # get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

	get 'auth/:provider/callback', to: 'sessions#create'
	get 'auth/failure', to: redirect('/')
	get 'signout', to: 'sessions#destroy', as: 'signout'

	resources :sessions, only: [:create, :destroy]

	mount Sidekiq::Web => '/sidekiq'
end

require 'ruby-progressbar'

namespace :readings do

	desc "Insert fake records into readings"	
	task :insert_fake_records => :environment do 
	
		for i in 1..1000 do 
			reading_id  = Reading.create(:tag_epc => 'fake #{i}', :antenna => 5) 
			sleep 5 
		end	

	end # task

end # namespace

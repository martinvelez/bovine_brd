package com.rfid.utils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class UIUtils
{

	/**
	 * Displays a dialog that has a yes and no button. Returns a number that indicates which button the user clicked.
	 * 
	 * @param options
	 * @param message
	 * @param title
	 * @param frame
	 * @return
	 */
	public static int displayDialog(Object[] options, String message, String title, JFrame frame)
	{
		int n = JOptionPane.showOptionDialog(frame, message, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
				null);
		return n;
	}
}

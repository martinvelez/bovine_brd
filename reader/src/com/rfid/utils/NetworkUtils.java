package com.rfid.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class NetworkUtils
{
	public static final String KEY = "dRrFOX3e12w9rWIvownq";

	public static final String HOST = "192.168.1.113";
	public static final String PATH = "/rfid/php_webapp/";

	public static final String UPLOAD_TAG_PATH = PATH + "set_tag.php";
	public static final String UPLOAD_POSITION_PATH = PATH + "set_positions.php";

	public static final String GET_TAGS_PATH = PATH + "get_tags.php";
	public static final String GET_COWS_PATH = PATH + "get_cows.php";
	public static final String GET_TAG_POSITIONS_PATH = PATH + "get_positions.php";

	/**
	 * Executes a request and returns the response.
	 * 
	 * @param url
	 * @return
	 */
	public static String executeRequest(final String url)
	{
		try
		{
			URL request = new URL(url);
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.openStream()));
			return getReaderContents(reader);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static String encodeUrl(String host, String path, String params)
	{
		URI uri;
		try
		{
			uri = new URI("http", host, path, params, null);
			String url = uri.toASCIIString();
			return url;
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static String getReaderContents(BufferedReader reader)
	{
		String output = "";
		String t = "";
		try
		{
			while ((t = reader.readLine()) != null)
			{
				output += t;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return output;
	}

	public static DeserializedTag[] fetchExperimentTags()
	{
		String uri = encodeUrl(HOST, GET_TAGS_PATH, "key=" + KEY);
		String response = NetworkUtils.executeRequest(uri);
		JSONArray parsedInput = new JSONArray(response);
		DeserializedTag[] output = new DeserializedTag[parsedInput.length()];

		for (int i = 0; i < parsedInput.length(); i++)
		{
			JSONObject serializedTag = parsedInput.getJSONObject(i);
			output[i] = new DeserializedTag(serializedTag);
		}

		return output;
	}

	public static String[] fetchCowList()
	{
		String uri = encodeUrl(HOST, GET_COWS_PATH, "key=" + KEY);
		String response = NetworkUtils.executeRequest(uri);
		return fetchNamesForRespose(response);
	}

	public static String[] fetchPositionList()
	{
		String uri = encodeUrl(HOST, GET_TAG_POSITIONS_PATH, "key=" + KEY);
		String response = NetworkUtils.executeRequest(uri);
		return fetchNamesForRespose(response);
	}

	/**
	 * 
	 * @param response
	 *            JSON-parceleable input that contains an array of JSON Objects who have a name key
	 * @return
	 */
	private static String[] fetchNamesForRespose(String response)
	{
		JSONArray parsedInput = new JSONArray(response);
		String output[] = new String[parsedInput.length()];
		for (int i = 0; i < output.length; i++)
		{
			JSONObject item = parsedInput.getJSONObject(i);
			output[i] = item.optString("name");
		}

		return output;
	}
}

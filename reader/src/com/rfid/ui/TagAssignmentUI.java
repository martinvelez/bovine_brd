package com.rfid.ui;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import com.rfid.experiment.ExperimentMain;
import com.rfid.utils.NetworkUtils;
import com.rfid.utils.UIUtils;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TagAssignmentUI
{

	private JFrame frame;
	private JTextField assignedTagNameTextField;
	private JLabel detectedTagLabel;
	private JButton nextTagButton;

	private final static String WAITING_TEXT = "Waiting for tag...";

	/**
	 * Create the application.
	 */
	public TagAssignmentUI()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public void tagDetected(String tagName)
	{
		detectedTagLabel.setText(tagName);
		nextTagButton.setEnabled(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lblNewLabel = new JLabel("To begin, number each tag and place it near an antenna so the reader can pick it up.");

		JLabel lblFoundTag = new JLabel("Found tag:");

		JLabel lblAssignTagName = new JLabel("Assign tag name:");

		assignedTagNameTextField = new JTextField();
		assignedTagNameTextField.setColumns(10);

		nextTagButton = new JButton("Next Tag");
		nextTagButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				uploadTagAssignment();
			}
		});

		nextTagButton.setEnabled(false);

		JButton btnFinishedAddingTags = new JButton("Finished Adding Tags");
		btnFinishedAddingTags.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				TagCowAssignment tgcow = new TagCowAssignment();
				frame.setVisible(false);
			}
		});

		detectedTagLabel = new JLabel(WAITING_TEXT);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.TRAILING)
												.addGroup(
														groupLayout
																.createSequentialGroup()
																.addGroup(
																		groupLayout
																				.createParallelGroup(Alignment.LEADING)
																				.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 414,
																						Short.MAX_VALUE)
																				.addComponent(nextTagButton)
																				.addGroup(
																						groupLayout
																								.createSequentialGroup()
																								.addGroup(
																										groupLayout
																												.createParallelGroup(
																														Alignment.LEADING)
																												.addComponent(lblAssignTagName)
																												.addComponent(lblFoundTag))
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addGroup(
																										groupLayout
																												.createParallelGroup(
																														Alignment.LEADING, false)
																												.addComponent(detectedTagLabel,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														Short.MAX_VALUE)
																												.addComponent(
																														assignedTagNameTextField,
																														GroupLayout.DEFAULT_SIZE,
																														116, Short.MAX_VALUE))
																								.addPreferredGap(ComponentPlacement.RELATED, 209,
																										Short.MAX_VALUE))).addContainerGap())
												.addGroup(groupLayout.createSequentialGroup().addComponent(btnFinishedAddingTags).addGap(131)))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblNewLabel)
						.addGap(18)
						.addGroup(
								groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblFoundTag)
										.addComponent(detectedTagLabel, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblAssignTagName)
										.addComponent(assignedTagNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)).addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(nextTagButton).addGap(18).addComponent(btnFinishedAddingTags).addContainerGap(98, Short.MAX_VALUE)));
		frame.getContentPane().setLayout(groupLayout);
	}

	private void uploadTagAssignment()
	{
		String tagAlias = assignedTagNameTextField.getText();

		String tagSerial = detectedTagLabel.getText();
		tagSerial = tagSerial.replaceAll(" ", "");

		String params = "key=" + NetworkUtils.KEY + "&serial=" + tagSerial + "&name=" + tagAlias;

		// Upload the tag serial / alias to server
		String requestUrl = NetworkUtils.encodeUrl(NetworkUtils.HOST, NetworkUtils.UPLOAD_TAG_PATH, params);
		String response = NetworkUtils.executeRequest(requestUrl);
		handleResponse(response);

		nextTagButton.setEnabled(true);
	}

	private void handleResponse(String response)
	{
		if (response != null && response.length() > 1)
		{
			int errorNum = Integer.parseInt(response.substring(0, 1));
			String errorMessage = response.substring(1);

			// Response of 0 indicates success uploading tag, response with a number followed by a message indicates error
			if (errorNum == 0)
			{
				detectedTagLabel.setText("Successfully uploaded tag.");
			}
			else
			{
				switch (errorNum)
				{
				case 2:
					// Generic database error
					break;
				case 3:
					// Display prompt to override name
					int result = UIUtils.displayDialog(new Object[]
					{ "Yes", "No" }, errorMessage, "Error", frame);

					if (result == JOptionPane.YES_OPTION)
					{
						String tagAlias = assignedTagNameTextField.getText();

						String tagSerial = detectedTagLabel.getText();
						tagSerial = tagSerial.replaceAll(" ", "");

						String params = "key=" + NetworkUtils.KEY + "&serial=" + tagSerial + "&name=" + tagAlias + "&override=true";
						detectedTagLabel.setText(errorMessage);

						// Upload the tag serial / alias to server
						String requestUrl = NetworkUtils.encodeUrl(NetworkUtils.HOST, NetworkUtils.UPLOAD_TAG_PATH, params);
						String secondResponse = NetworkUtils.executeRequest(requestUrl);
						handleResponse(secondResponse);
					}

					break;
				case 4:
					// Display prompt to indicate that tag name cannot be overridden
					UIUtils.displayDialog(new Object[]
					{ "Ok" }, "Cannot override tag name. Please choose a different name.", "Error", frame);
					break;
				}
			}
		}
	}
}

package com.rfid.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.UIManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rfid.experiment.ExperimentMain;
import com.rfid.utils.DeserializedTag;
import com.rfid.utils.NetworkUtils;

public class TagCowAssignment
{

	private JFrame frame;
	private JComboBox<DeserializedTag> tagComboBox;
	private JComboBox<String> cowComboBox, positionComboBox;
	private JButton btnAssign;
	private JTable table;

	/**
	 * Create the application.
	 */
	public TagCowAssignment()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private Object[][] tableData;
	private String[] columnNames =
	{ "Tag", "Cow", "Tag Placement" };

	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		DeserializedTag[] tags = NetworkUtils.fetchExperimentTags();
		tagComboBox = new JComboBox<DeserializedTag>(tags);

		cowComboBox = new JComboBox<String>();
		cowComboBox.setEditable(true);

		positionComboBox = new JComboBox<String>();
		positionComboBox.setEditable(true);

		// Populate the cow and position combo boxes from database data
		final String[] cowList = NetworkUtils.fetchCowList();
		addItemsToComboBox(cowList, cowComboBox);

		final String[] positionList = NetworkUtils.fetchPositionList();
		addItemsToComboBox(positionList, positionComboBox);

		btnAssign = new JButton("Assign");
		btnAssign.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String tagName = ((DeserializedTag) tagComboBox.getSelectedItem()).getAlias();
				String cowName = (String) cowComboBox.getSelectedItem();
				String positionName = (String) positionComboBox.getSelectedItem();

				int currentlySelectedIndex = tagComboBox.getSelectedIndex();
				// Update the table (add 1 to currentlySelectedIndex to account for header)
				tableData[currentlySelectedIndex + 1][0] = tagName;
				tableData[currentlySelectedIndex + 1][1] = cowName;
				tableData[currentlySelectedIndex + 1][2] = positionName;
				table.updateUI();

				int nextTagIndex = Math.min(currentlySelectedIndex + 1, tagComboBox.getItemCount() - 1);
				tagComboBox.setSelectedIndex(nextTagIndex);
				cowComboBox.setSelectedIndex(-1);
				cowComboBox.setSelectedItem("");
				positionComboBox.setSelectedIndex(-1);
				positionComboBox.setSelectedItem("");

				boolean didCowNameExist = addItemToComboBox(cowName, cowComboBox);
				boolean didPositionExist = addItemToComboBox(positionName, positionComboBox);

				// Now upload the list of cows / positions that were added
				if (!didCowNameExist)
				{
					
				}
				
				if (!didPositionExist)
				{
					
				}
			}
		});

		// Construct a table with n + 1 rows (+ 1 for header) and m columns
		tableData = new Object[tags.length + 1][columnNames.length];
		table = new JTable(tableData, columnNames);
		for (int i = 0; i < columnNames.length; i++)
		{
			tableData[0][i] = columnNames[i];
		}
		table.updateUI();

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout
				.createParallelGroup(Alignment.TRAILING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.LEADING)
												.addGroup(
														groupLayout.createSequentialGroup().addContainerGap()
																.addComponent(table, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
												.addGroup(
														groupLayout
																.createSequentialGroup()
																.addGap(49)
																.addComponent(tagComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(cowComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(positionComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE))).addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap(187, Short.MAX_VALUE).addComponent(btnAssign).addGap(184)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addGap(29)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.BASELINE)
										.addComponent(tagComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(cowComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(positionComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)).addGap(18).addComponent(btnAssign).addGap(26)
						.addComponent(table, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE).addContainerGap()));
		frame.getContentPane().setLayout(groupLayout);
	}

	/**
	 * Adds item to a combo box if the item does not exist already.
	 * 
	 * @param item
	 * @param comboBox
	 * @return false if the item could not be added because it already existed in the comboBox
	 */
	public boolean addItemToComboBox(String item, JComboBox<String> comboBox)
	{
		for (int i = 0; i < comboBox.getItemCount(); i++)
		{
			String checkItem = comboBox.getItemAt(i);
			// If item already exists, don't add it
			if (checkItem != null && checkItem.equals(item))
			{
				return false;
			}
		}

		comboBox.addItem(item);
		return true;
	}

	/**
	 * Adds items from an array to a combo box, but only if the item in the array is not already in the combo box list.
	 * 
	 * @param item
	 * @param comboBox
	 */
	public void addItemsToComboBox(String[] items, JComboBox<String> comboBox)
	{
		for (int j = 0; j < items.length; j++)
		{
			boolean addToList = true;
			for (int i = 0; i < comboBox.getItemCount(); i++)
			{
				String checkItem = comboBox.getItemAt(i);
				// If item already exists, don't add it
				if (checkItem != null && checkItem.equals(items[j]))
				{
					addToList = false;
					break;
				}
			}

			if (addToList)
			{
				comboBox.addItem(items[j]);
			}
		}
	}
}

# Cow Grooming Tracker

Healthy cattle readily use grooming brushes but this behavior subsides when
animals become ill. Tracking use of a brush may provide an opportunity for
health monitoring, especially if the process could be automated. We assessed
how healthy heifers groom themselves on a brush and hypothesized that
radiofrequency identification (RFID) could be used to accurately and
automatically record this behavior.

The computer software, a Java 7 program written using the Alien Software
Development Kit (v2.3.1; Alien Technology), communicated with the RFID reader
via the network. Every second, the RFID reader sent data to the software about
which tags were sensed by the antennas. The software then parsed and stored the
data on local and remote PostgreSQL 9.3 databases. A web application, built
using Ruby-on-Rails 4, provided real-time views of the data and implemented
functions to assist in processing and analyzing the data. Data were
post-processed such that intervals (up to a maximum of 16 s) with no RFID
detections but contained between 2 readings were considered positive for the
entire duration (animal in brush proximity).

## What does this repository contain?

This repository contains the source for the main components of a cow grooming
tracking system we setup.
All of this was completed as part of a research project to study the feasibility
of automatically tracking cow grooming behavior using radio-frequencey 
identification (RFID) technology.

Main Components:

1. Tag Listener: A Java program that uses the Alien RFID SDK to listen to tag readings.  It stores the readings in a PostgreSQL database.
2. Ruby-on-Rails Web Application: This application provides a user interface to see current readings and to help analize data. 

Miscellaneous Components:

3. Alien Reader SDK: The software development kit provided by Alien in the form of Java JAR files.
4. SDK Examples: Simple example programs I wrote to test the Alien Reader SDK.


## Barn Setup

Black and white CCTV video cameras (model no. WV-BP334, Panasonic Corporation
of North America, Secaucus, NJ) and lenses (model no. 13VG2812ASII, Tamron,
Commack, NY) were connected to a digital video recorder with digital
surveillance software (GeoVi- sion Surveillance System V8.5 Inc., Taipei,
Taiwan). To enable video recording during low-light conditions, red holiday
lights suspended from pen rafters operated on a timer between 1600 and 0800 h.
Eight cameras focused on the brush (suspended 2.0 to 2.2 m above). Each camera
was set to continuously record at medium quality and 30 frames/s.

An RFID reader (ALR-9900+, Alien Technology, Morgan Hill, CA) was attached to
the wall of the pen above the brush. Four 25- × 25-cm panel antennas (model no.
S9028PCL96RTN, Laird USA, Earth City, MO) were mounted 2 m above the ground
from wood beams attached to the rafters, in each quadrant around the brush, at
a distance ranging from 0.6 to 1.3 m from its center (Figure 1, marked with
asterisks), and set to an attenuation level of 25 to confine the reading range
to a relatively small radius around the brush. Ultra-high-frequency RFID chips
(ALN-9629, Alien Technology, Morgan Hill, CA) were attached to each existing
left and right plastic identification ear tag using a single layer of duct
tape.


![Barn Setup](images/barn_setup.jpg)

![Cow Grooming](images/cow.jpg)





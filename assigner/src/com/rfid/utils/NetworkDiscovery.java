package com.rfid.utils;
import com.alien.enterpriseRFID.discovery.AlienDiscoveryUnknownReaderException;
import com.alien.enterpriseRFID.discovery.DiscoveryItem;
import com.alien.enterpriseRFID.discovery.DiscoveryListener;
import com.alien.enterpriseRFID.discovery.NetworkDiscoveryListenerService;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;

public class NetworkDiscovery implements DiscoveryListener
{
	NetworkDiscoveryListenerService listener;
	DiscoveryItem discoveredReader;

	public NetworkDiscovery() throws Exception
	{
		listener = new NetworkDiscoveryListenerService();
		listener.setDiscoveryListener(this);
		listener.startService();

		while (listener.isRunning())
		{
			Thread.sleep(100);
		}
		System.out.println("Stopped listening for reader");
	}

	@Override
	public void readerAdded(DiscoveryItem reader)
	{
		discoveredReader = reader;
		System.out.println("Reader discovered   " + reader.toString());
		listener.stopService();

	}

	@Override
	public void readerRemoved(DiscoveryItem reader)
	{
		System.out.println("Reader removed!   " + reader.toString());
	}

	@Override
	public void readerRenewed(DiscoveryItem reader)
	{
		discoveredReader = reader;
		System.out.println("Reader renewed!   " + reader.toString());

	}

	public AlienClass1Reader getReader() throws AlienDiscoveryUnknownReaderException
	{
		return discoveredReader.getReader();
	}

}
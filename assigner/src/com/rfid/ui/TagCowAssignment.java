package com.rfid.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TagCowAssignment
{

	private JFrame frame;
	private JComboBox<String> tagComboBox, cowComboBox, positionComboBox;
	private JButton btnAssign;
	private JTable table;

	/**
	 * Create the application.
	 */
	public TagCowAssignment()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private Object[][] tableData;
	private String[] columnNames =
	{ "Tag", "Cow", "Tag Placement" };

	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		tagComboBox = new JComboBox<String>();

		cowComboBox = new JComboBox<String>();
		cowComboBox.setEditable(true);

		positionComboBox = new JComboBox<String>();
		positionComboBox.setEditable(true);

		btnAssign = new JButton("Assign");
		btnAssign.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String tagName = (String) tagComboBox.getSelectedItem();
				String cowName = (String) cowComboBox.getSelectedItem();
				String positionName = (String) positionComboBox.getSelectedItem();

				tagComboBox.setSelectedIndex(-1);
				cowComboBox.setSelectedIndex(-1);
				positionComboBox.setSelectedIndex(-1);

				addItemToComboBox(cowName, cowComboBox);
				addItemToComboBox(positionName, positionComboBox);

			}
		});

		table = new JTable(tableData, columnNames);

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout
				.createParallelGroup(Alignment.TRAILING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.LEADING)
												.addGroup(
														groupLayout.createSequentialGroup().addContainerGap()
																.addComponent(table, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
												.addGroup(
														groupLayout
																.createSequentialGroup()
																.addGap(49)
																.addComponent(tagComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(cowComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(positionComboBox, GroupLayout.PREFERRED_SIZE, 100,
																		GroupLayout.PREFERRED_SIZE))).addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap(187, Short.MAX_VALUE).addComponent(btnAssign).addGap(184)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addGap(29)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.BASELINE)
										.addComponent(tagComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(cowComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(positionComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)).addGap(18).addComponent(btnAssign).addGap(26)
						.addComponent(table, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE).addContainerGap()));
		frame.getContentPane().setLayout(groupLayout);
	}

	/**
	 * Adds item to a combo box if the item does not exist already.
	 * 
	 * @param item
	 * @param comboBox
	 */
	public void addItemToComboBox(String item, JComboBox<String> comboBox)
	{
		for (int i = 0; i < comboBox.getItemCount(); i++)
		{
			String checkItem = comboBox.getItemAt(i);
			// If item already exists, don't add it
			if (checkItem != null && checkItem.equals(item))
			{
				return;
			}
		}

		comboBox.addItem(item);
	}
}

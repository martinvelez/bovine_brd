package com.rfid.ui;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import com.rfid.experiment.ExperimentMain;
import com.rfid.utils.NetworkUtils;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TagAssignmentUI
{

	private JFrame frame;
	private JTextField assignedTagNameTextField;
	private JLabel detectedTagLabel;
	private JButton nextTagButton;

	private final static String WAITING_TEXT = "Waiting for tag...";

	/**
	 * Create the application.
	 */
	public TagAssignmentUI()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public void tagDetected(String tagName)
	{
		detectedTagLabel.setText(tagName);
		nextTagButton.setEnabled(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lblNewLabel = new JLabel("To begin, number each tag and place it near an antenna so the reader can pick it up.");

		JLabel lblFoundTag = new JLabel("Found tag:");

		JLabel lblAssignTagName = new JLabel("Assign tag name:");

		assignedTagNameTextField = new JTextField();
		assignedTagNameTextField.setColumns(10);

		nextTagButton = new JButton("Next Tag");
		nextTagButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				String tagAlias = assignedTagNameTextField.getText();
				String tagSerial = detectedTagLabel.getText();
				String requestUrl = ExperimentMain.UPLOAD_URL + "&tagSerial=" + tagSerial + "&tagAlias=" + tagAlias;
				String response = NetworkUtils.executeRequest(requestUrl);

				detectedTagLabel.setText(response);
			}
		});

		nextTagButton.setEnabled(false);

		JButton btnFinishedAddingTags = new JButton("Finished Adding Tags");
		btnFinishedAddingTags.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				TagCowAssignment tgcow = new TagCowAssignment();
				frame.setVisible(false);
			}
		});

		detectedTagLabel = new JLabel(WAITING_TEXT);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.TRAILING)
												.addGroup(
														groupLayout
																.createSequentialGroup()
																.addGroup(
																		groupLayout
																				.createParallelGroup(Alignment.LEADING)
																				.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 414,
																						Short.MAX_VALUE)
																				.addComponent(nextTagButton)
																				.addGroup(
																						groupLayout
																								.createSequentialGroup()
																								.addGroup(
																										groupLayout
																												.createParallelGroup(
																														Alignment.LEADING)
																												.addComponent(lblAssignTagName)
																												.addComponent(lblFoundTag))
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addGroup(
																										groupLayout
																												.createParallelGroup(
																														Alignment.LEADING, false)
																												.addComponent(detectedTagLabel,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														Short.MAX_VALUE)
																												.addComponent(
																														assignedTagNameTextField,
																														GroupLayout.DEFAULT_SIZE,
																														116, Short.MAX_VALUE))
																								.addPreferredGap(ComponentPlacement.RELATED, 209,
																										Short.MAX_VALUE))).addContainerGap())
												.addGroup(groupLayout.createSequentialGroup().addComponent(btnFinishedAddingTags).addGap(131)))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblNewLabel)
						.addGap(18)
						.addGroup(
								groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblFoundTag)
										.addComponent(detectedTagLabel, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblAssignTagName)
										.addComponent(assignedTagNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)).addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(nextTagButton).addGap(18).addComponent(btnFinishedAddingTags).addContainerGap(98, Short.MAX_VALUE)));
		frame.getContentPane().setLayout(groupLayout);
	}
}

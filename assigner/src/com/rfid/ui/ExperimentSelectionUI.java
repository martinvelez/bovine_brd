package com.rfid.ui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.rfid.experiment.ExperimentSetup;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ExperimentSelectionUI
{

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public ExperimentSelectionUI()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JTextArea txtrWelcomeSelectWhether = new JTextArea();
		txtrWelcomeSelectWhether.setEditable(false);
		txtrWelcomeSelectWhether.setFont(new Font("Monospaced", Font.PLAIN, 12));
		txtrWelcomeSelectWhether.setLineWrap(true);
		txtrWelcomeSelectWhether.setText("Welcome! Select whether you would like to begin a saved experiment, or set up a new one.");

		JButton btnLoadExperiment = new JButton("Load Experiment");

		JButton btnBeginNewExperiment = new JButton("Begin New Experiment");
		btnBeginNewExperiment.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				try
				{
					ExperimentSetup beginSetup = new ExperimentSetup();
					frame.setVisible(false);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addGroup(
												groupLayout.createSequentialGroup()
														.addComponent(txtrWelcomeSelectWhether, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
														.addContainerGap())
										.addGroup(
												Alignment.TRAILING,
												groupLayout
														.createSequentialGroup()
														.addGroup(
																groupLayout
																		.createParallelGroup(Alignment.TRAILING, false)
																		.addComponent(btnLoadExperiment, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																		.addComponent(btnBeginNewExperiment, Alignment.LEADING,
																				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
														.addGap(165)))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(
				groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(txtrWelcomeSelectWhether, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(18).addComponent(btnLoadExperiment).addGap(18).addComponent(btnBeginNewExperiment)
						.addContainerGap(130, Short.MAX_VALUE)));
		frame.getContentPane().setLayout(groupLayout);
	}
}

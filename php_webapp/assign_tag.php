<?php

require_once("login_funcs.php");
require_once("tag_funcs.php");

if(!checkLogin()) {
	echo "1LOGIN KEY ERROR!!!";
	die;
}

if(isset($_REQUEST["tagName"]) && isset($_REQUEST["cowName"]) && isset($_REQUEST["positionName"])) {
	echo assignTagToCowWithPosition($_REQUEST["tagName"], $_REQUEST["cowName"], $_REQUEST["positionName"]);
} else {
	echo "2Error: serial and tag name not present in URL";
}

?>
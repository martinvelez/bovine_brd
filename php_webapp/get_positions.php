<?php

require_once("login_funcs.php");
require_once("position_funcs.php");

if(!checkLogin()) {
	echo "1";
	die;
}

echo json_encode(getPositions());

?>
<?php

require_once("db_funcs.php");
require_once("cow_funcs.php");
require_once("position_funcs.php");

function getTags()
{
	dbLogin();

	$tagsResult = mysql_query(mysql_escape_string("SELECT * FROM tags"));

	$tags = array();
	while($row = mysql_fetch_row($tagsResult)) {
		$tag = array();
		$tag["id"] = $row[0];
		$tag["serial"] = $row[1];
		$tag["name"] = $row[2];
		$tag["cow_id"] = $row[3];
		$tag["position_id"] = $row[4];

		$tags[] = $tag;
	}

	return $tags;
}

function setTag($serial, $name, $override)
{
	dbLogin();

	$existingTags = getTags();
	
	$id = -1;
	for($i = 0; $i < count($existingTags); $i++) {
		$tag = $existingTags[$i];
		if($tag["serial"] == $serial) {
			$id = $tag["id"];
		}
		
		if($tag["name"] == $name) {
			return "4Tag with name already exists. Choose a different name";
		}
	}
	
	if($id == -1) {
		$success = mysql_query("INSERT INTO tags (serial, name) VALUES ('$serial', '$name')");
	} else {
		if($override) {
			$success = mysql_query("UPDATE tags SET name='$name' WHERE id=$id");
		} else {
			return "3Tag with serial number already exists. Would you like to override?";
		}
	}
	
	if($success) {
		return "0Tag created";
	} else {
		return "2Error adding tag to database";
	}
}


// Precondition all names in tags, cows, and positions are unique
function assignTagToCowWithPosition($tagName, $cowName, $positionName) 
{
	
	// Find the tag ID in the database
	$tags = getTags();
	$tagId = -1;
	foreach($tags as $tag) {
		if($tag["name"] == $tagName) {
			$tagId = $tag["id"];
		}
	}
	if($tagId == -1) {
		return "4Tag to assign does not exist";
	}
	
	// Find the cow ID in the database
	$cows = getCows();
	$cowId = -1;
	foreach($cows as $cow) {
		if($cow["name"] == $cowName) {
			$cowId = $cow["id"];
		}
	}
	if($cowId == -1) {
		return "5Cow to assign to does not exist";
	}
	
	// Find the position ID in the database
	$positions = getPositions();
	$positionId = -1;
	foreach($positions as $position) {
		if($position["name"] == $positionName) {
			$positionId = $position["id"];
		}
	}
	if($positionId == -1) {
		return "6Tag position does not exist";
	}
	
	
	$success = mysql_query("UPDATE tags SET cow_id=$cowId,position_id=$positionId WHERE id=$tagId");
	if($success) {
		return "0Tag $tagName is attached to $cowName on the $positionName";
	} else {
		return "3Error assigning tag in database";
	}
}

?>
/**
 * 
 */
package edu.ucdavis.edu.SDKExamples;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.alien.enterpriseRFID.reader.AlienClass1Reader;

/**
 * @author marvelez
 *
 */
public class AlienClass1Communicator {

	/**
	 * Constructor
	 */
	public AlienClass1Communicator() throws Exception {
		
		Discoverer discoverer = new Discoverer();
		AlienClass1Reader reader = discoverer.getReader();
		reader.setUsername("alien");
		reader.setPassword("password");
		reader.open();
		reader.setAntennaSequence("0 1 2 3");
		
	  // Use stdin for user input
	  BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

	  do {
	    System.out.print("\nAlien>"); // Show prompt
	    String line = in.readLine(); // Grab user input
	    if (line.equals("q")) break; // Quit when "q" is pressed
	    System.out.println(reader.doReaderCommand(line)); // Send command, print result
	  } while (true); // Repeat indefinitely

	  System.out.println("\nGoodbye.");
	  reader.close(); // Close the reader connection
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	  try {
		    new AlienClass1Communicator();
		  } catch(Exception e) {
		    System.out.println("Error: " + e.toString());
		  }

	}

}

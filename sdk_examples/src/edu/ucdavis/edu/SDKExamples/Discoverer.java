/**
 * This class will listen to the network for readers.  We assume that there 
 * will only be one.  Once we find it, we store it.  Then some other class uses
 * the reader.
 */
package edu.ucdavis.edu.SDKExamples;

import com.alien.enterpriseRFID.discovery.AlienDiscoveryUnknownReaderException;
import com.alien.enterpriseRFID.discovery.DiscoveryItem;
import com.alien.enterpriseRFID.discovery.DiscoveryListener;
import com.alien.enterpriseRFID.discovery.NetworkDiscoveryListenerService;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;

/**
 * @author marvelez
 *
 */
public class Discoverer implements DiscoveryListener {
	NetworkDiscoveryListenerService listener;
	DiscoveryItem discoveredReader;

	public Discoverer() throws Exception
	{
		listener = new NetworkDiscoveryListenerService();
		listener.setDiscoveryListener(this);
		listener.startService();

		while (listener.isRunning()){
			Thread.sleep(1000);
		}
		System.out.println("Stopped listening for reader");
	}
	
	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerAdded(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerAdded(DiscoveryItem discoveryitem) {
		discoveredReader = discoveryitem;
		System.out.println("Reader discovered\n" + discoveredReader.toString());
		listener.stopService();
	}

	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerRemoved(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerRemoved(DiscoveryItem discoveryItem) {
		System.out.println("Reader Removed:\n" + discoveryItem.toString());
	}

	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerRenewed(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerRenewed(DiscoveryItem discoveryItem) {
		System.out.println("Reader Renewed:\n" + discoveryItem.toString());
	}

	public AlienClass1Reader getReader() throws AlienDiscoveryUnknownReaderException
	{
		return discoveredReader.getReader();
	}
}
